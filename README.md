This is a maven plugin to scan through the REST API class files and generate the "client interface" / "request classes" / "response classes".
Those things will be supplied to Retrofit to create REST client which may be used to do REST tests.


# How to use it

### Include the plugin to your maven build
```xml
<plugin>
    <groupId>com.atlassian</groupId>
    <artifactId>retrofit-plugin</artifactId>
    <version>0.1.1</version>
    <configuration>
        <scanningPackage>com.atlassian.servicedesk.internal.rest</scanningPackage>
        <generatedSourceCodeFolder>../tests/sd-rest-client/src/main/java</generatedSourceCodeFolder>
    </configuration>
    <executions>
        <execution>
            <id>generate-rest-clients</id>
            <phase>prepare-package</phase>
            <goals>
                <goal>touch</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

scanningPackage - where to scan your REST API classes
generatedSourceCodeFolder - where to save the generated source codes

### Run mvn prepare-package -DskipTests to generate the source codes

### Consume the generated classes
```java
// Create the client
RestClientBuilder<AgentsPageResource> builder = new RestClientBuilder<AgentsPageResource>();
builder.setEndpoint(BASE_URL);
builder.setCredentials("admin", "admin");
AgentsPageResource restClient = builder.buildFor(AgentsPageResource.class);

// Use it
JsonObject searchResponse = restClient.searchAgents("PROJECT_KEY_KUDOS", "AGENT_NAME_PATRICIA");
```
Note: You'll need to add some extra dependencies to your maven module where those codes live
```xml
<dependency>
    <groupId>com.squareup.retrofit</groupId>
    <artifactId>retrofit</artifactId>
    <version>1.9.0</version>
</dependency>
<dependency>
    <groupId>com.atlassian</groupId>
    <artifactId>retrofit-plugin</artifactId>
    <version>0.1.1</version>
</dependency>
```

# What's going on behind the scene

### Maven settings is collected by RestScannerMojo

### Scan the REST API class files: see "scanner" and "scanresult" packages
- The class files are scan through using Javassist and Reflections to gather metadata: url path, HTTP method, path param, query param ...
They are the values of corresponding annotations
- The request (payload) is a little bit different. It is parse to jsonSchema using jackson-module-jsonSchema and jackson-module-scala

### Use metadata to generate source codes: see "generator" package
- The REST client interfaces are generated using forge-roaster library.
- Request classes are generated using jsonschema2pojo library

### REST client builder: see "client" package