package com.atlassian.rest.retrofit.scanner.impl;

import com.atlassian.rest.retrofit.scanner.ScannerContext;
import com.atlassian.rest.retrofit.scanresult.impl.ClassScanResult;
import com.atlassian.rest.retrofit.scanresult.impl.MethodScanResult;
import com.atlassian.rest.retrofit.util.AnnotationUtil;
import com.google.common.collect.Sets;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import org.apache.commons.lang3.ClassUtils;
import org.apache.maven.plugin.logging.Log;
import org.reflections.adapters.JavassistAdapter;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;
import java.util.Set;

public class ClassScanner
{
    private static final Set<String> CLASS_ANNOTATION_TYPES = Sets.newHashSet(
            Produces.class.getCanonicalName(),
            Path.class.getCanonicalName(),
            Consumes.class.getCanonicalName()
    );

    private static final ClassScanner classScanner = new ClassScanner();

    public static ClassScanner get()
    {
        return classScanner;
    }

    private ClassScanner() {}

    public ClassScanResult scan(ClassFile classFile, ScannerContext scannerContext)
    {
        JavassistAdapter adapter = scannerContext.getAdapter();
        String className = adapter.getClassName(classFile);
        Log logger = scannerContext.getLogger();

        Set<String> classAnnotations = Sets.newHashSet(adapter.getClassAnnotationNames(classFile));
        Set<String> classDetectionResult = Sets.intersection(classAnnotations, CLASS_ANNOTATION_TYPES);

        if (!classDetectionResult.isEmpty())
        {
            logger.debug("Detected class " + className + " ...");

            AnnotationsAttribute attrs = (AnnotationsAttribute) classFile.getAttribute(AnnotationsAttribute.visibleTag);
            String path = AnnotationUtil.getAnnotationValue(attrs.getAnnotation(Path.class.getCanonicalName()));
            List<String> consumes = AnnotationUtil.getAnnotationValues(attrs.getAnnotation(Consumes.class.getCanonicalName()));
            List<String> produces = AnnotationUtil.getAnnotationValues(attrs.getAnnotation(Produces.class.getCanonicalName()));

            List<MethodScanResult> methods = MethodScanner.get().scan(adapter.getMethods(classFile), scannerContext);

            return new ClassScanResult.Builder()
                                    .packageName(ClassUtils.getPackageCanonicalName(className))
                                    .className(ClassUtils.getShortClassName(className))
                                    .path(path)
                                    .consumes(consumes)
                                    .produces(produces)
                                    .methods(methods)
                                    .build();
        }
        else
        {
            logger.debug("Skipped class " + className + " ...");
            return ClassScanResult.skip();
        }
    }
}
