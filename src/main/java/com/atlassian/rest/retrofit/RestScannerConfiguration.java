package com.atlassian.rest.retrofit;

import com.google.common.collect.Sets;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.logging.Log;

import java.net.URL;
import java.util.Set;

public class RestScannerConfiguration
{
    private Set<URL> urls;
    private Log logger;
    private String scanningPackage;
    private Set<Artifact> dependencyArtifacts;
    private String generatedSourceCodeFolder;

    private RestScannerConfiguration() {}

    public Set<URL> getUrls()
    {
        return urls;
    }

    public Log getLogger()
    {
        return logger;
    }

    public String getScanningPackage()
    {
        return scanningPackage;
    }

    public Set<Artifact> getDependencyArtifacts()
    {
        return dependencyArtifacts;
    }

    public String getGeneratedSourceCodeFolder()
    {
        return generatedSourceCodeFolder;
    }

    public static class Builder
    {
        private Set<URL> urls = Sets.newHashSet();
        private Log logger;
        private String scanningPackage;
        private Set<Artifact> dependencyArtifacts;
        private String generatedSourceCodeFolder;

        public Builder logger(Log logger)
        {
            this.logger = logger;
            return this;
        }

        public Builder urls(Set<URL> urls)
        {
            this.urls = urls;
            return this;
        }

        public Builder scanningPackage(String scanningPackage)
        {
            this.scanningPackage = scanningPackage;
            return this;
        }

        public Builder dependencyArtifacts(Set<Artifact> dependencyArtifacts)
        {
            this.dependencyArtifacts = dependencyArtifacts;
            return this;
        }

        public Builder generatedSourceCodeFolder(String generatedSourceCodeFolder)
        {
            this.generatedSourceCodeFolder = generatedSourceCodeFolder;
            return this;
        }

        public RestScannerConfiguration build()
        {
            RestScannerConfiguration instance = new RestScannerConfiguration();
            instance.urls = this.urls;
            instance.logger = this.logger;
            instance.scanningPackage = this.scanningPackage;
            instance.dependencyArtifacts = this.dependencyArtifacts;
            instance.generatedSourceCodeFolder = this.generatedSourceCodeFolder;
            return instance;
        }
    }
}
