package com.atlassian.rest.retrofit.client;

import com.google.common.collect.Maps;
import retrofit.ErrorHandler;
import retrofit.Profiler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.Log;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.Client;
import retrofit.client.Client.Provider;
import retrofit.converter.Converter;

import java.util.Base64;
import java.util.Map;
import java.util.concurrent.Executor;

public class RestClientBuilder<T>
{
    private String endpoint;
    private Client client;
    private Executor httpExecutor;
    private Executor callbackExecutor;
    private Converter converter;
    private Profiler profiler;
    private ErrorHandler errorHandler;
    private Log log;
    private LogLevel logLevel = LogLevel.NONE;

    private String username;
    private String password;

    private Map<String, String> headers = Maps.newHashMap();

    public RestClientBuilder() {}

    public RestClientBuilder setEndpoint(String endpoint)
    {
        this.endpoint = endpoint;
        return this;
    }

    public RestClientBuilder setClient(Client client)
    {
        this.client = client;
        return this;
    }

    public RestClientBuilder setClientProvider(Provider clientProvider)
    {
        this.client = clientProvider.get();
        return this;
    }

    public RestClientBuilder setConverter(Converter converter)
    {
        this.converter = converter;
        return this;
    }

    public RestClientBuilder setErrorHandler(ErrorHandler errorHandler)
    {
        this.errorHandler = errorHandler;
        return this;
    }

    public RestClientBuilder setExecutors(Executor httpExecutor, Executor callbackExecutor)
    {
        this.httpExecutor = httpExecutor;
        this.callbackExecutor = callbackExecutor;
        return this;
    }

    public RestClientBuilder setLog(Log log)
    {
        this.log = log;
        return this;
    }

    public RestClientBuilder setLogLevel(LogLevel logLevel)
    {
        this.logLevel = logLevel;
        return this;
    }

    public RestClientBuilder setProfiler(Profiler profiler)
    {
        this.profiler = profiler;
        return this;
    }

    public RestClientBuilder setCredentials(String username, String password)
    {
        this.username = username;
        this.password = password;
        return this;
    }

    public RestClientBuilder addHeader(String name, String value)
    {
        this.headers.put(name, value);
        return this;
    }

    public T buildFor(Class<T> clientClass)
    {
        if (endpoint == null)
        {
            throw new IllegalArgumentException("Endpoint must not be null.");
        }

        if (clientClass == null)
        {
            throw new IllegalArgumentException("Client class must not be null.");
        }

        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(endpoint);

        if (client != null)
        {
            builder.setClient(client);
        }

        if (converter != null)
        {
            builder.setConverter(converter);
        }

        if (errorHandler != null)
        {
            builder.setErrorHandler(errorHandler);
        }

        if (httpExecutor != null)
        {
            builder.setExecutors(httpExecutor, callbackExecutor);
        }

        if (log != null)
        {
            builder.setLog(log);
        }

        if (logLevel != null)
        {
            builder.setLogLevel(logLevel);
        }

        if (profiler != null)
        {
            builder.setProfiler(profiler);
        }

        builder.setRequestInterceptor(new RequestInterceptor()
        {
            @Override
            public void intercept(RequestFacade request)
            {
                // Set this header first then no one can overwrite it
                if (username != null && password != null)
                {
                    String credentials = username + ":" + password;
                    String string = "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes());
                    request.addHeader("Authorization", string);
                }

                for (Map.Entry<String, String> header : headers.entrySet())
                {
                    request.addHeader(header.getKey(), header.getValue());
                }
            }
        });

        RestAdapter adapter = builder.build();
        return adapter.create(clientClass);
    }
}
