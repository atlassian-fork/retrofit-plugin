package com.atlassian.rest.retrofit.scanresult.impl;

import com.atlassian.rest.retrofit.scanresult.OptionalScanResult;

import java.util.List;

public class MethodScanResult extends OptionalScanResult
{
    private String type;
    private String path;
    private String name;
    private List<String> produces;
    private List<ParamScanResult> params;

    private MethodScanResult() {}

    public static MethodScanResult skip()
    {
        MethodScanResult skippedInstance = new MethodScanResult();
        skippedInstance.isSkipped = true;
        return skippedInstance;
    }

    public String getPath()
    {
        return path;
    }

    public String getName()
    {
        return name;
    }

    public String getType()
    {
        return type;
    }

    public List<String> getProduces()
    {
        return produces;
    }

    public List<ParamScanResult> getParams()
    {
        return params;
    }

    public static class Builder
    {
        private String type;
        private String path;
        private String name;
        private List<String> produces;
        private List<ParamScanResult> params;

        public Builder path(String path)
        {
            this.path = path;
            return this;
        }

        public Builder name(String name)
        {
            this.name = name;
            return this;
        }

        public Builder type(String type)
        {
            this.type = type;
            return this;
        }

        public Builder produces(List<String> produces)
        {
            this.produces = produces;
            return this;
        }

        public Builder params(List<ParamScanResult> paramScanResultList)
        {
            this.params = paramScanResultList;
            return this;
        }

        public MethodScanResult build()
        {
            MethodScanResult instance = new MethodScanResult();
            instance.path = this.path;
            instance.type = this.type;
            instance.name = this.name;
            instance.produces = this.produces;
            instance.params = this.params;
            return instance;
        }
    }
}
